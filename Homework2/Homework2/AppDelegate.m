//
//  AppDelegate.m
//  Homework2
//
//  Created by Andrii Tymchenko on 3/1/16.
//  Copyright © 2016 Andrii Tymchenko. All rights reserved.
//

#import "AppDelegate.h"
#import "Example.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
//    
//    Задание:
//    
//    1. Я очень хочу чтобы вы попрактиковались создавать и использовать enum списки. Они ОЧЕНЬ распространены, они делают код красивее и вносят дополнительную информацию. Это просто очень хорошая практика их использовать! Практикуйтесь! Создайте кучу разных пропертей под разные энумы. Постарайтесь усвоить это сразу.
//    
//    2. Надо попрактиковаться со структурами. Например такое небольшое задание:
//    на поле 10х10 рандомно создайте точек (разберитесь как рандомно генерировать цифры, подсказка - функция arc4random()) и проверяйте какая из точек попадает в квадрат размером 3х3 в самом центре поля. Грубо говоря надо определить какие из точек в массиве попадают в центр и вывести их на печать. Пробуйте и задавайте вопросы.
//    
//    3. Разберитесь с оболочками NSNumber и NSValue. На самом деле тут все предельно просто, но вы должны понять саму суть.
//
//    
    /*
     BOOL boolVar = YES;
     NSInteger intVar = 4;
     NSUInteger uIntVar =6;
     CGFloat floatVar = 4.5f;
     double doubleVar = 4.6f; //чого doubleVar = 4.599999904632568 ?????
     
     NSNumber *boolObject = [NSNumber numberWithBool:boolVar];
     NSNumber *intObject = [NSNumber numberWithInteger:intVar];
     NSNumber *uIntObject = [NSNumber numberWithUnsignedInteger:uIntVar];
     NSNumber *floatObject = [NSNumber numberWithFloat:floatVar];
     NSNumber *doubleObject = [NSNumber numberWithDouble:doubleVar];
     
     NSLog(@"boolVar = %@, intVar = %@, uIntVar = %@, floatVar = %@, doubleVar = %@",boolObject, intObject, uIntObject, floatObject, doubleObject);
     
     */

    /*
    Example *cot = [[Example alloc] init];
    
    cot.color = Blue;
    cot.position = Middle;
    cot.mood = Super;
    
    NSLog(@"cot color = %d, cot position = %d, cot mood = %d",cot.color, cot.position, cot.mood);
    // як вивести його значення а не цифру?
    */
//   це (походу) точно не потрібно але так більше непотрібної інфи
//    CGSize sizeArea = CGSizeMake(10, 10);
//    CGRect rectSquare = CGRectMake(<#CGFloat x#>, <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)
//    
    CGPoint point1 = [self addPointrandom];
    CGPoint point2 = [self addPointrandom];
    CGPoint point3 = [self addPointrandom];
    CGPoint point4 = [self addPointrandom];
    CGPoint point5 = [self addPointrandom];
    
    NSArray *array =
    [NSArray arrayWithObjects:
     [NSValue valueWithCGPoint:point1],
     [NSValue valueWithCGPoint:point2],
     [NSValue valueWithCGPoint:point3],
     [NSValue valueWithCGPoint:point4],
     [NSValue valueWithCGPoint:point5],
     nil];
    
    for(NSValue *value in array) {
        
        CGPoint point = [value CGPointValue];
        
        if (point.x > 3 & point.x < 7 & point.y < 3 & point.y < 7) {
            NSLog(@"point %@ in square",NSStringFromCGPoint(point));
        }
        
    }
    
    // ідеї не мої,але я зміг їх відтворити через добу після побачення
    
    
    
    
    
   
    return YES;
}

- (CGPoint) addPointrandom {
    
    CGPoint point = CGPointMake(((arc4random()%10)+ 0), ((arc4random()% 10) + 0));
    return point;
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
