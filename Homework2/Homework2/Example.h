//
//  Example.h
//  Homework2
//
//  Created by Andrii Tymchenko on 3/1/16.
//  Copyright © 2016 Andrii Tymchenko. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    
    Black,
    Blue,
    Red,
    White,
    Purple,
    Yellow
    
}Color;

typedef enum{
    
    Front,
    Middle,
    Back
    
}Position;

typedef enum{
    
    Good,
    Fine,
    Nice,
    Bad,
    VeryBad,
    Awful,
    Gorgeous,
    Incredible,
    Fantastic,
    Super,
    
}Mood;

@interface Example : NSObject

@property(assign,nonatomic) Color color;
@property(assign,nonatomic) Position position;
@property(assign,nonatomic) Mood mood;

@end
