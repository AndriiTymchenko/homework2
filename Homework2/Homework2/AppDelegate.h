//
//  AppDelegate.h
//  Homework2
//
//  Created by Andrii Tymchenko on 3/1/16.
//  Copyright © 2016 Andrii Tymchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

